import {expect} from 'chai';
import NumbersValidator from '../numbers-validator.js';

describe('NumbersValidator', () => {
  let validator;

  beforeEach(() => {
    validator = new NumbersValidator();
  });

  describe('isNumberEven', () => {
    it('should return true for even numbers', () => {
      expect(validator.isNumberEven(2)).to.be.true;
      expect(validator.isNumberEven(0)).to.be.true;
      expect(validator.isNumberEven(-4)).to.be.true;
    });

    it('should return false for odd numbers', () => {
      expect(validator.isNumberEven(1)).to.be.false;
      expect(validator.isNumberEven(-3)).to.be.false;
    });

    it('should throw an error for non-number inputs', () => {
      expect(() => validator.isNumberEven('2')).to.throw(Error, '[2] is not of type "Number" it is of type "string"');
      expect(() => validator.isNumberEven(null)).to.throw(Error, '[null] is not of type "Number" it is of type "object"');
      expect(() => validator.isNumberEven(undefined)).to.throw(Error, '[undefined] is not of type "Number" it is of type "undefined"');
    });
  });

  describe('getEvenNumbersFromArray', () => {
    it('should return an array of even numbers', () => {
      expect(validator.getEvenNumbersFromArray([1, 2, 3, 4, 5, 6])).to.deep.equal([2, 4, 6]);
      expect(validator.getEvenNumbersFromArray([0, -2, 7, 9, -10])).to.deep.equal([0, -2, -10]);
    });

    it('should throw an error for non-array inputs', () => {
      expect(() => validator.getEvenNumbersFromArray('not an array')).to.throw(Error, '[not an array] is not an array of "Numbers"');
      expect(() => validator.getEvenNumbersFromArray(123)).to.throw(Error, '[123] is not an array of "Numbers"');
    });

    it('should throw an error for arrays containing non-number elements', () => {
      expect(() => validator.getEvenNumbersFromArray([1, 2, '3'])).to.throw(Error, '[[1,2,"3"]] is not an array of "Numbers"');
    });
  });

  describe('isAllNumbers', () => {
    it('should return true if all elements are numbers', () => {
      expect(validator.isAllNumbers([1, 2, 3])).to.be.true;
      expect(validator.isAllNumbers([0, -2, 3.5])).to.be.true;
    });

    it('should return false if any element is not a number', () => {
      expect(validator.isAllNumbers([1, '2', 3])).to.be.false;
      expect(validator.isAllNumbers([null, 2, 3])).to.be.false;
    });

    it('should throw an error for non-array inputs', () => {
      expect(() => validator.isAllNumbers('not an array')).to.throw(Error, '[not an array] is not an array');
      expect(() => validator.isAllNumbers(123)).to.throw(Error, '[123] is not an array');
    });
  });

  describe('isInteger', () => {
    it('should return true for integer numbers', () => {
      expect(validator.isInteger(1)).to.be.true;
      expect(validator.isInteger(0)).to.be.true;
      expect(validator.isInteger(-3)).to.be.true;
    });

    it('should return false for non-integer numbers', () => {
      expect(validator.isInteger(1.5)).to.be.false;
      expect(validator.isInteger(-3.7)).to.be.false;
    });

    it('should throw an error for non-number inputs', () => {
      expect(() => validator.isInteger('1')).to.throw(Error, '[1] is not a number');
      expect(() => validator.isInteger(null)).to.throw(Error, '[null] is not a number');
      expect(() => validator.isInteger(undefined)).to.throw(Error, '[undefined] is not a number');
    });
  });
});
